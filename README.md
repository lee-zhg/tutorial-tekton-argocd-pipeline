# Tutorial for Tekton and ArgoCD Integration - with bitbucket

This tutorial shows the concept of seperation of concerns in DevOps.

Three repos (conceptually, not all teams will have write access to all of them) are used for the exercise.

- This git repo contains the pipeline setup and other deployment information
    * https://bitbucket.org/lee-zhg/tutorial-tekton-argocd-pipeline.git
- This git repo contains the application source code
    * https://bitbucket.org/lee-zhg/tutorial-tekton-argocd-code.git
- The git repo that contains only the infrastructure manifests (yaml)
    * https://bitbucket.org/lee-zhg/tutorial-tekton-argocd-infra.git

- We will be using Tekton to build the container image, and update the Infra Git repo.
- We will be using ArgoCD to deploy the application from the infra git repo

![overview-diagram](./images/tekton-argocd.png)

Note: this repo is based on the IBM Garage assets.


## Setup Git repositories

You need all three repos for the exercise. You clone one repo on your local environment.

- Get a copy of the devops git repository and change directory
  ```bash
  git clone https://bitbucket.org/lee-zhg/tutorial-tekton-argocd-pipeline.git

  cd tutorial-tekton-argocd-pipeline
  ```


## Setup Cluster

- You need an OpenShift 4 cluster, you can use [CodeReadyContainers (CRC)](https://access.redhat.com/documentation/en-us/red_hat_codeready_containers), [OpenShift PlayGround](https://learn.openshift.com/playgrounds/openshift42/), or OpenShift from a cloud provider like IBM Cloud

- Install OpenShift Operators
  - Install OpenShift Pipeline Operator

    ```bash
    oc apply -f operators/tekton-operator.yaml
    ```

  - Install [ArgoCD (HELM) Operator](https://github.com/disposab1e/argocd-operator-helm) on the `argocd` namespace.

    ```bash
    oc apply -f operators/argocd-operator.yaml
    ```

    >Note: make sure you wait until the argocd-operator is finished before installing the argocd-cr..or it will fail. You can do this:

    ```bash
    oc get ClusterServiceVersion -n argocd
    
    NAME                                   DISPLAY                        VERSION   REPLACES   PHASE
    argocd-operator.v0.0.8                 Argo CD                        0.0.8                Succeeded
    ```
    
    and wait for the "succeeded" to come up before proceeding.
    
    ```bash
    oc apply -f operators/argocd-cr.yaml
    ```
    
    and wait for the argocd server Pod to be running

    ```
    oc get pods -n argocd -l app.kubernetes.io/name=example-argocd-server
    ```
    
    ```
    NAME                                     READY   STATUS    RESTARTS   AGE
    example-argocd-server-57c4fd5c45-zf4q6   1/1     Running   0          115s
    ```


## Setup CLIs
- [Install Tekton CLI](https://github.com/tektoncd/cli#installing-tkn) `tkn`
- [Install ArgoCD CLI](https://argoproj.github.io/argo-cd/cli_installation/) `argocd`


## Setup target Namespace

- Create a new namespace/project

  ```bash
  oc new-project tekton-argocd
  ```

- Set the environment variable `NAMESPACE`

  ```bash
  export NAMESPACE=$(oc project -q)
  echo "NAMESPACE set to $NAMESPACE"
  ```


## Create ArgoCD Application

- Set an environment variable `ARGOCD_URL` using the route

    ```bash
    export ARGOCD_NAMESPACE="argocd"
    export ARGOCD_SERVER=$(oc get route example-argocd-server -n $ARGOCD_NAMESPACE -o jsonpath='{.spec.host}')
    export ARGOCD_URL="https://$ARGOCD_SERVER"
    echo ARGOCD_URL=$ARGOCD_URL
    echo ARGOCD_SERVER=$ARGOCD_SERVER
    ```

- Login into the UI.

    ```bash
    open $ARGOCD_URL
    ```

- Use `admin` as the username and get the password with the following command

    ```bash
    oc get secret example-argocd-cluster -n $ARGOCD_NAMESPACE -o jsonpath='{.data.admin\.password}' | base64 -d
    ```

    For example the output is similar to this:

    ```
    tyafMb7BNvO0kP9eizx3CojrK8pYJFQq
    ```

    ```bash
    export ARGOCD_PASSWORD=$(oc get secret example-argocd-cluster -n $ARGOCD_NAMESPACE -o jsonpath='{.data.admin\.password}' | base64 -d)
    ```

- Login into ArgoCD

  ```bash
  argocd login --username admin --password $ARGOCD_PASSWORD $ARGOCD_SERVER
  ```

  >Note: if you have problems with DNS using CRC, you might need to edit `/etc/hosts` and add entry for the `$ARGOCD_SERVER` with IP Address from `crc ip`

- Create the App in ArgoCD using your own Infra git repository

  ```bash
  export GIT_REPOSITORY_URL=https://bitbucket.org/lee-zhg/tutorial-tekton-argocd-infra.git
  ```

  ```bash
  export ARGOCD_APP=$(oc project -q)
  export GIT_MANIFEST_DIR="yamls/ocp"
  ```

  ```bash
  argocd app create $ARGOCD_APP \
  --project default \
  --repo $GIT_REPOSITORY_URL \
  --path $GIT_MANIFEST_DIR \
  --dest-server https://kubernetes.default.svc \
  --dest-namespace $NAMESPACE \
  --sync-policy automated \
  --self-heal \
  --auto-prune
  ```

  ```
  application 'tekton-argocd' created
  ```


## Build Image with Tekton

- Deploy the pipeline assets, edit the `pipelines/git.yaml` and `pipelines/pipeline-build-git.yaml` and use your own Code and Infra git repositories respectively.

  ```bash
  oc apply -f pipeline/ -n $NAMESPACE
  ```

  ```bash
  tkn resources ls -n $NAMESPACE
  ```
  ```
  NAME     TYPE    DETAILS
  source   git     url: https://github.com/ibm-cloud-architecture/tutorial-tekton-argocd-code
  image    image   url: image-registry.openshift-image-registry.svc:5000/$NAMESPACE/app
  ```

  ```bash
  tkn task ls -n $NAMESPACE
  ```
  ```
  NAME        AGE
  build       36 seconds ago
  build-git   36 seconds ago
  ```

  ```bash
  tkn pipeline ls -n $NAMESPACE
  ```
  ```
  NAME        AGE              LAST RUN   STARTED   DURATION   STATUS
  build-git   17 seconds ago   ---        ---       ---        ---
  ```

- Run the build task to test image build only
  ```bash
  tkn task start build \
    -i image=image \
    -i source=source \
    --showlog \
    -s pipeline \
    -n $NAMESPACE
  ```

  ```bash
  tkn taskrun ls -n $NAMESPACE
  ```
  ```
  NAME              STARTED         DURATION    STATUS
  build-run-c4d4r   4 minutes ago   2 minutes   Succeeded
  ```

  ```bash
  oc get imagestream -n $NAMESPACE
  ```
  ```
  NAME   IMAGE REPOSITORY                                                     TAGS      UPDATED
  app    image-registry.openshift-image-registry.svc:5000/tekton-argocd/app   b711ac7   About a minute ago
  ```


## Update Infrastructure Git repo

### Configure Bitbucket Personal Access Token

Personal access tokens can be used in `Bitbucket Data Center` and `Server` in place of passwords for Git over HTTPS, or to authenticate when using the Bitbucket REST API. Detail is available at https://confluence.atlassian.com/bitbucketserver/personal-access-tokens-939515499.html.

Collect the bitbucket personal access token.


###  Create Secret

You should have collect the bitbucket personal access token. Alternatively, you use the password of your bitbucket cloud account.

- Create the secret for the Infra repository, replace `<GIT_USERNAME>` and `<GIT_TOKEN>`, keep the quotes

    ```bash
    export GIT_USERNAME='<GIT_USERNAME>'
    export GIT_TOKEN='<GIT_TOKEN>'
    ```

    or 

    ```
    export GIT_USERNAME='<GIT_USERNAME>'
    export GIT_TOKEN='<GIT_PASSWORD>'
    ```

    ```bash
    oc create secret generic git-infra-secret \
      --from-literal=username="$GIT_USERNAME" \
      --from-literal=token="$GIT_TOKEN" \
      -n $NAMESPACE
    ```

- Run Pipeline to Build Image and Update Infra Git repo
  ```bash
  tkn pipeline start build-git \
              --showlog \
              -r source=source \
              -r image=image \
              -s pipeline \
              -n $NAMESPACE
  ```

  ```bash
  tkn pipeline ls -n $NAMESPACE
  ```
  ```
  NAME        AGE             LAST RUN              STARTED         DURATION   STATUS
  build-git   2 minutes ago   build-git-run-rdhmj   2 minutes ago   1 minute   Succeeded
  ```


## Extra Credit: Create Git Webhook (come soon)

- This is only possible if your OpenShift cluster is accessible from the the github server (ie github.com)
- Create a WebHook for the Code Git repo
  ```bash
  oc apply -f triggers/ -n $NAMESPACE
  ```

  ```bash
  oc create route edge --service=el-cicd -n $NAMESPACE
  ```

  ```bash
  export GIT_WEBHOOK_URL=$(oc get route el-cicd -o jsonpath='{.spec.host}' -n $NAMESPACE)
  echo "https://$GIT_WEBHOOK_URL"
  ```

- Set the `GIT_REPO_NAME` to name of the Code Git repo like `tutorial-tekton-argocd-code`
  ```bash
  export GIT_REPO_NAME='<GIT_REPO_NAME>'
  ```

- Set the `GIT_REPO_OWNER` to name of the Code Git repo like `ibm-cloud-architecture`
  ```bash
  export GIT_REPO_OWNER='<GIT_REPO_OWNER>'
  ```

- Run curl to create the web hook
  ```bash
  curl -v -X POST -u $GIT_USERNAME:$GIT_TOKEN \
  -d "{\"name\": \"web\",\"active\": true,\"events\": [\"push\"],\"config\": {\"url\": \"https://$GIT_WEBHOOK_URL\",\"content_type\": \"json\",\"insecure_ssl\": \"0\"}}" \
  -L https://api.github.com/repos/$GIT_REPO_OWNER/$GIT_REPO_NAME/hooks
  ```

Make a change on the Code repository, and verify that Github sent the WebHook to the event listener, and that the Pipeline runs in OpenShift Console
